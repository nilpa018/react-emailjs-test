import { ContactUs } from "./components/ContactUs";
import "./App.css";

function App() {
  return (
    <>
      <h1>Envoyer un email avec React et EmailJS</h1>
      <div className="card">
        <ContactUs />
      </div>
    </>
  );
}

export default App;
