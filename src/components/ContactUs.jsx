import React, { useRef, useState } from "react";
import emailjs from "@emailjs/browser";

export const ContactUs = () => {
  const [isSuccessMessage, setIsSuccessMessage] = useState(false);
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();
    grecaptcha.ready(function () {
      grecaptcha
        .execute(import.meta.env.VITE_CAPTCHAT_CLIENT_PUBLIC_KEY, {
          action: "submit",
        })
        .then(() => {
          emailjs
            .sendForm(
              import.meta.env.VITE_EMAILJS_SERVICEID,
              import.meta.env.VITE_EMAILJS_TEMPLATEID,
              form.current,
              {
                publicKey: import.meta.env.VITE_EMAILJS_PUBLIC_KEY,
              }
            )
            .then(
              () => {
                setIsSuccessMessage(true);
                displayMessage();
                form.current.reset();
              },
              (error) => {
                console.log(`Une erreur s'est produite : ${error.text}`);
              }
            );
        });
    });
  };

  const displayMessage = () => {
    setTimeout(() => {
      setIsSuccessMessage(false);
    }, 5000);
  };

  const messageSend = (
    <div id="sended-message">
      "Félicitation, l'email à été envoyé avec succès !"
    </div>
  );

  return (
    <>
      {isSuccessMessage && messageSend}
      <div className="email-form">
        <form ref={form} onSubmit={sendEmail}>
          <label>Votre nom</label>
          <input type="text" name="user_name" />
          <br /><br />
          <label>Email du destinataire</label>
          <input type="email" name="user_email" />
          <br /><br />
          <label>Votre message</label>
          <textarea name="message" />
          <br /><br />
          <input className="button" type="submit" value="Envoyer" />
        </form>
      </div>
    </>
  );
};
